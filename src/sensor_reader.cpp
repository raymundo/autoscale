#include <Streaming.h>
#include "NTPClient/fixedpoint_math.h"

#include "sensor_reader.h"

SensorReader::SensorReader()
    : hx711multi(CHANNEL_COUNT, DOUTS, CLK)
{
  // int count, byte *dout, byte pd_sck, byte gain = 128

  queue_handle = xQueueCreate(1000, sizeof(sensorsample_t));
  if (queue_handle == 0)
  {
    Serial << "xQueueCreate failed." << endl;
  }
  configASSERT(queue_handle);
}

SensorReader::~SensorReader()
{
  vQueueDelete(queue_handle);
  queue_handle = nullptr;
}

void SensorReader::AcquireSample(ufixedpoint64_t current_timestamp_as_unix_epoch)
{
  sensorsample_t sample;
  sample.timestamp = current_timestamp_as_unix_epoch;
  hx711multi.readRaw(results);
  sample.values[0] = results[0];
  sample.values[1] = results[1];
  sample.values[2] = results[2];
  sample.values[3] = results[3];

  while (xQueueSendToBack(queue_handle, &sample, 0) == errQUEUE_FULL)
  {
    sensorsample_t dummy_sample;
    xQueueReceive(queue_handle, &dummy_sample, 0);
  }

  acquired_samples++;

  if (acquired_samples % 300 == 0)
  {
    Serial << current_timestamp_as_unix_epoch.formatAsEpoch()
           << " - total samples acquired: " << acquired_samples
           << endl;
  }
}

uint32_t SensorReader::TotalAcquiredSamples() const
{
  return acquired_samples;
}

sensorsample_t SensorReader::RemoveOldestSample()
{
  sensorsample_t result;
  xQueueReceive(queue_handle, &result, portMAX_DELAY);
  return result;
}
