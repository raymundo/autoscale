#pragma once

class WiFiUDP;
class IPAddress;

#include "fixedpoint_math.h"

class NTPClock
{
private:
  WiFiUDP &udp;

  uint32_t last_micros_query_result;
  uint64_t internal_microseconds;
  uint32_t internal_clock_overflows;
  ufixedpoint64_t unix_epoch;
  fixedpoint64_t pending_delta_for_unix_epoch;
  fixedpoint64_t unapplied_delta_secs_for_offset_correction;
  fixedpoint64_t acceptable_transmission_delay;
  bool ntp_request_in_flight;
  uint64_t internal_timestamp_of_last_ntp_request;
  uint64_t internal_timestamp_of_last_timeprint;
  uint32_t ntp_requests_sent;
  uint32_t ntp_requests_received;

public:
  NTPClock(WiFiUDP &udp);
  virtual ~NTPClock() = default;

  ufixedpoint64_t tick();
  bool isSynced() const;
  uint32_t requests_sent() const;
  uint32_t requests_received() const;

  static String formatEpoch(const ufixedpoint64_t& unix_epoch);

private:
  void sendNTPpacket(IPAddress &address);
  void update_internal_microseconds();
};
