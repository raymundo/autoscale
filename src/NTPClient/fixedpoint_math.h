#pragma once

#include <cstdint>

class String;
struct fixedpoint64_t;

struct ufixedpoint64_t
{
  union {
    uint64_t value;
    struct
    {
      uint32_t fract;
      uint32_t trunc;
    };
  };

  static ufixedpoint64_t from_bits(const uint64_t value);

  ufixedpoint64_t();
  ufixedpoint64_t(const uint32_t trunc, const uint32_t fract);
  bool operator==(const ufixedpoint64_t &rhs) const;
  ufixedpoint64_t operator+(const ufixedpoint64_t &rhs) const;
  ufixedpoint64_t &operator+=(const ufixedpoint64_t &rhs);
  ufixedpoint64_t &operator+=(const fixedpoint64_t &rhs);
  fixedpoint64_t operator-(const ufixedpoint64_t &rhs) const;
  String toString() const;
  String formatAsEpoch() const;
};

struct fixedpoint64_t
{
  union {
    int64_t value;
    struct
    {
      uint32_t fract;
      int32_t trunc;
    };
  };

  static fixedpoint64_t from_float(const float value);
  static fixedpoint64_t from_bits(const int64_t value);

  fixedpoint64_t();
  fixedpoint64_t(const int32_t trunc, const uint32_t fract);
  bool operator==(const fixedpoint64_t &rhs) const;
  bool operator<=(const fixedpoint64_t &rhs) const;
  fixedpoint64_t operator+(const fixedpoint64_t &rhs) const;
  fixedpoint64_t operator*(const fixedpoint64_t &rhs) const;
  fixedpoint64_t &operator+=(const fixedpoint64_t &rhs);
  fixedpoint64_t operator-(const fixedpoint64_t &rhs) const;
  fixedpoint64_t operator-() const;
  fixedpoint64_t operator>>(const int &bits) const;
  String toString() const;
};
