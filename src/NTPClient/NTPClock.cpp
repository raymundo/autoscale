#include <WiFi.h>
#include <WiFiUdp.h>
#include <cmath>
#include <Streaming.h>

#include "NTPClock.h"
#include "ntp_packet.h"

const char *NTP_SERVER_NAME = "europe.pool.ntp.org";
const int MICROSECONDS_BETWEEN_NTP_REQUESTS = 5000000;

// Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
const unsigned long SEVENTY_YEARS_IN_SECONDS = 2208988800UL;

const uint32_t SECONDS_PER_HOUR = 60 * 60;
const uint32_t SECONDS_PER_DAY = 24 * SECONDS_PER_HOUR;
const uint32_t SECONDS_PER_YEAR = 365 * SECONDS_PER_DAY;

ufixedpoint64_t secs_since_1900_to_unix_epoch(const ufixedpoint64_t &secs_since_1900)
{
  return ufixedpoint64_t(secs_since_1900.trunc - SEVENTY_YEARS_IN_SECONDS, secs_since_1900.fract);
}

ufixedpoint64_t unix_epoch_to_secs_since_1900(const ufixedpoint64_t &unix_epoch)
{
  return ufixedpoint64_t(unix_epoch.trunc + SEVENTY_YEARS_IN_SECONDS, unix_epoch.fract);
}

NTPClock::NTPClock(WiFiUDP &udp)
    : udp(udp), last_micros_query_result(0), internal_microseconds(0), internal_clock_overflows(0), acceptable_transmission_delay(1000, 0), ntp_request_in_flight(false), internal_timestamp_of_last_ntp_request(0), internal_timestamp_of_last_timeprint(0), ntp_requests_sent(0), ntp_requests_received(0)
{
  update_internal_microseconds();
}

void NTPClock::update_internal_microseconds()
{
  int64_t delta_micros;
  {
    auto current_micros_query_result = micros();
    delta_micros = (int64_t)current_micros_query_result - (int64_t)last_micros_query_result;
    if (delta_micros < 0)
    {
      static_assert(sizeof(current_micros_query_result) < sizeof(delta_micros), "sizeof(current_micros_query_result) < sizeof(delta_micros)");
      delta_micros += (1 << sizeof(current_micros_query_result));
      internal_clock_overflows++;
    }
    last_micros_query_result = current_micros_query_result;
  }
  internal_microseconds += delta_micros;

  // advance UTC time (unix_epoch)
  {
    fixedpoint64_t delta_secs = fixedpoint64_t::from_bits((delta_micros << 32) / 1000000);

    // regular clock advancement
    unix_epoch += delta_secs;

    // apply fraction of pending clock offset from NTP
    if (pending_delta_for_unix_epoch.value != 0)
    {
      // (1/2^4 seconds change per second of real time)
      fixedpoint64_t fraction_of_pending_delta = (delta_secs + unapplied_delta_secs_for_offset_correction) >> 4;
      fraction_of_pending_delta = fraction_of_pending_delta * pending_delta_for_unix_epoch;
      if (fraction_of_pending_delta == fixedpoint64_t())
      {
        unapplied_delta_secs_for_offset_correction += delta_secs;
      }
      else
      {
        unapplied_delta_secs_for_offset_correction = fixedpoint64_t();
        unix_epoch += fraction_of_pending_delta;
        pending_delta_for_unix_epoch = pending_delta_for_unix_epoch - fraction_of_pending_delta;
      }
    }
    else
    {
      unapplied_delta_secs_for_offset_correction = fixedpoint64_t();
    }
  }
}

ufixedpoint64_t NTPClock::tick()
{
  update_internal_microseconds();

  // time to send another NTP request?
  if ((internal_timestamp_of_last_ntp_request == 0) ||
      (internal_timestamp_of_last_ntp_request + MICROSECONDS_BETWEEN_NTP_REQUESTS < internal_microseconds))
  {
    //resolve a random server from the pool
    IPAddress timeServerIP;
    WiFi.hostByName(NTP_SERVER_NAME, timeServerIP);

    // send an NTP packet to a time server
    sendNTPpacket(timeServerIP);

    internal_timestamp_of_last_ntp_request = internal_microseconds;
    ntp_request_in_flight = true;

    if (requests_sent() % 10 == 0)
    {
      Serial << unix_epoch.formatAsEpoch()
             << " - NTP packets sent: " << requests_sent() << " received: " << requests_received()
             << endl;
    }
  }

  if (internal_timestamp_of_last_timeprint + 1000000 < internal_microseconds)
  {
    internal_timestamp_of_last_timeprint = internal_microseconds;
    //Serial << unix_epoch.formatAsEpoch() << "   pending_delta: " << pending_delta_for_unix_epoch.toString() << endl;
  }

  if (ntp_request_in_flight)
  {
    // check for NTP answer
    int cb = udp.parsePacket();
    if (cb)
    {
      ntp_request_in_flight = false;

      ntp_requests_received++;

      if (requests_received() % 10 == 0)
      {
        Serial << unix_epoch.formatAsEpoch()
               << " - NTP packets sent: " << requests_sent() << " received: " << requests_received()
               << endl;
      }

      // We've received a packet, read the data from it
      // read the packet
      ntp_packet_t ntp_packet;
      udp.read((uint8_t *)&ntp_packet, sizeof(ntp_packet_t));

      // this is NTP time (seconds since Jan 1 1900)
      ufixedpoint64_t t1(ntohl(ntp_packet.origTm_s), ntohl(ntp_packet.origTm_f));
      t1 = secs_since_1900_to_unix_epoch(t1);
      ufixedpoint64_t t2(ntohl(ntp_packet.rxTm_s), ntohl(ntp_packet.rxTm_f));
      t2 = secs_since_1900_to_unix_epoch(t2);
      ufixedpoint64_t t3(ntohl(ntp_packet.txTm_s), ntohl(ntp_packet.txTm_f));
      t3 = secs_since_1900_to_unix_epoch(t3);
      ufixedpoint64_t t4(unix_epoch);
      fixedpoint64_t transmission_delay = (t4 - t1) - (t3 - t2);
      fixedpoint64_t clock_offset = ((t2 - t1) + (t3 - t4)) >> 1;

      //Serial << "NTP packet received:" << endl;
      //Serial << "t1: " << t1.toString() << " t2: " << t2.toString() << endl;
      //Serial << "t3: " << t3.toString() << " t4: " << t4.toString() << endl;
      //Serial << "transmission_delay: " << transmission_delay.toString() << " clock_offset: " << clock_offset.toString() << endl;

      if (abs(clock_offset.trunc) > 3)
      {
        unix_epoch = t3;
        unix_epoch += transmission_delay;
        pending_delta_for_unix_epoch = fixedpoint64_t();
      }
      else
      {
        if (transmission_delay.value < acceptable_transmission_delay.value)
        {
          acceptable_transmission_delay = transmission_delay;
        }

        fixedpoint64_t threshold = acceptable_transmission_delay * fixedpoint64_t::from_float(1.5f);
        if (transmission_delay <= threshold)
        {
          pending_delta_for_unix_epoch = clock_offset;
        }
        else
        {
          //Serial.print("Too high transmission_delay: ");
          //Serial.print(transmission_delay.toString());
          //Serial.print(" > ");
          //Serial.print(threshold.toString());
          //Serial.print(" from: ");
          //Serial.println(acceptable_transmission_delay.toString());

          // re-request in 1 second
          internal_timestamp_of_last_ntp_request -= MICROSECONDS_BETWEEN_NTP_REQUESTS;
          internal_timestamp_of_last_ntp_request += 1000000;
        }
      }
    }
  }

  return unix_epoch;
}

bool NTPClock::isSynced() const
{
  return unix_epoch.trunc > 1500000000ULL;
}

uint32_t NTPClock::requests_sent() const
{
  return ntp_requests_sent;
}

uint32_t NTPClock::requests_received() const
{
  return ntp_requests_received;
}

// send an NTP request to the time server at the given address
void NTPClock::sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  ntp_packet_t ntp_packet;
  memset(&ntp_packet, 0, sizeof(ntp_packet_t));
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  ntp_packet.li_vn_mode = 0b11100011; // LI, Version, Mode
  ntp_packet.stratum = 0;             // Stratum, or type of clock
  ntp_packet.poll = 6;                // Polling Interval
  ntp_packet.precision = 0xEC;        // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  ntp_packet.rootDelay = 0;
  ntp_packet.rootDispersion = 0;
  ntp_packet.refId = 0;
  ufixedpoint64_t secs_since_1900 = unix_epoch_to_secs_since_1900(unix_epoch);
  ntp_packet.txTm_s = htonl(secs_since_1900.trunc);
  ntp_packet.txTm_f = htonl(secs_since_1900.fract);

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write((const uint8_t *)&ntp_packet, sizeof(ntp_packet_t));
  udp.endPacket();

  ntp_requests_sent++;
}
