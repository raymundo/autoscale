#include "fixedpoint_math.h"
#include <Arduino.h>
#include <cmath>
#include <ctime>

ufixedpoint64_t::ufixedpoint64_t()
    : value(0)
{
}

ufixedpoint64_t::ufixedpoint64_t(const uint32_t trunc, const uint32_t fract)
    : fract(fract), trunc(trunc)
{
}

ufixedpoint64_t ufixedpoint64_t::from_bits(const uint64_t value)
{
  ufixedpoint64_t result;
  result.value = value;
  return result;
}

bool ufixedpoint64_t::operator==(const ufixedpoint64_t &rhs) const
{
  return value == rhs.value;
}

ufixedpoint64_t ufixedpoint64_t::operator+(const ufixedpoint64_t &rhs) const
{
  return ufixedpoint64_t::from_bits(value + rhs.value);
}

ufixedpoint64_t &ufixedpoint64_t::operator+=(const ufixedpoint64_t &rhs)
{
  value += rhs.value;
  return *this;
}

ufixedpoint64_t &ufixedpoint64_t::operator+=(const fixedpoint64_t &rhs)
{
  value += rhs.value;
  return *this;
}

fixedpoint64_t ufixedpoint64_t::operator-(const ufixedpoint64_t &rhs) const
{
  return fixedpoint64_t::from_bits((int64_t)(value - rhs.value));
}

String ufixedpoint64_t::toString() const
{
  uint32_t f = fract / 4295;
  if (f > 99999)
  {
    return String(trunc) + "." + String(f);
  }
  else if (f > 9999)
  {
    return String(trunc) + ".0" + String(f);
  }
  else if (f > 999)
  {
    return String(trunc) + ".00" + String(f);
  }
  else if (f > 99)
  {
    return String(trunc) + ".000" + String(f);
  }
  else if (f > 9)
  {
    return String(trunc) + ".0000" + String(f);
  }
  else
  {
    return String(trunc) + ".00000" + String(f);
  }
}

String ufixedpoint64_t::formatAsEpoch() const
{
  const size_t buffer_size = (10 + 1 + 8) + (1 + 6) + 10;
  char buffer[buffer_size];

  time_t rawtime = trunc;
  struct tm *ptm = std::gmtime(&rawtime);
  std::strftime(buffer, buffer_size, "%F %T", ptm);

  uint32_t micros = fract;
  //micros /= 4294967;
  micros /= 4295;
  sprintf(&buffer[10 + 1 + 8], ".%06u", micros);

  return String(buffer);
}

fixedpoint64_t::fixedpoint64_t()
    : value(0)
{
}

fixedpoint64_t fixedpoint64_t::from_float(const float value)
{
  float_t fractpart, intpart;
  fractpart = std::modf(value, &intpart);
  return fixedpoint64_t((int32_t)intpart, (uint32_t)(fractpart * 4294967296.0));
}

fixedpoint64_t fixedpoint64_t::from_bits(const int64_t value)
{
  fixedpoint64_t result;
  result.value = value;
  return result;
}

fixedpoint64_t::fixedpoint64_t(const int32_t trunc, const uint32_t fract)
    : fract(fract), trunc(trunc)
{
}

bool fixedpoint64_t::operator==(const fixedpoint64_t &rhs) const
{
  return value == rhs.value;
}

bool fixedpoint64_t::operator<=(const fixedpoint64_t &rhs) const
{
  return value <= rhs.value;
}

fixedpoint64_t fixedpoint64_t::operator+(const fixedpoint64_t &rhs) const
{
  return fixedpoint64_t::from_bits(value + rhs.value);
}

fixedpoint64_t fixedpoint64_t::operator*(const fixedpoint64_t &rhs) const
{
  return fixedpoint64_t::from_bits((value >> 16) * (rhs.value >> 16));
}

fixedpoint64_t &fixedpoint64_t::operator+=(const fixedpoint64_t &rhs)
{
  value += rhs.value;
  return *this;
}

fixedpoint64_t fixedpoint64_t::operator-(const fixedpoint64_t &rhs) const
{
  return fixedpoint64_t::from_bits(value - rhs.value);
}

fixedpoint64_t fixedpoint64_t::operator-() const
{
  return fixedpoint64_t::from_bits(-value);
}

fixedpoint64_t fixedpoint64_t::operator>>(const int &bits) const
{
  return fixedpoint64_t::from_bits(value >> bits);
}

String fixedpoint64_t::toString() const
{
  if (value < 0)
  {
    return String("-") + ufixedpoint64_t::from_bits((uint64_t)(-value)).toString();
  }
  else
  {
    return ufixedpoint64_t::from_bits((uint64_t)(value)).toString();
  }
}
