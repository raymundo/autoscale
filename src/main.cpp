#include <WiFi.h>
#include <WiFiUdp.h>
#include <Streaming.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include "NTPClient/NTPClock.h"
#include "sensor_reader.h"

// accessing the WiFi
const char *ssid = "Sesamstrasse";
const char *pass = "Rumpelstilzchen";

// accessing the MySQL server
const IPAddress sql_addr(192, 168, 0, 1);
char sql_user[] = "autoscale";
char sql_pass[] = "autoscale";

WiFiUDP udp;

void ensureWiFiConnection()
{
  while (!WiFi.isConnected())
  {
    // local port to listen for UDP packets
    const unsigned int localPort = 2390;

    Serial << "Connecting to " << ssid << endl;
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.print(".");
    }
    Serial << endl
           << "WiFi connected" << endl;
    Serial << "IP address: " << WiFi.localIP() << endl;
    udp.begin(localPort);
    Serial << "UDP Local port: " << localPort << endl;
  }
}

MySQL_Cursor *ensureSQLConnection()
{
  static WiFiClient wifi_client;
  static MySQL_Connection mySQL_connection(&wifi_client);
  static MySQL_Cursor *pMySQL_cursor = nullptr;

  while (!mySQL_connection.connected())
  {
    if (pMySQL_cursor != nullptr)
    {
      delete pMySQL_cursor;
      pMySQL_cursor = nullptr;
    }

    Serial.print("Connecting to SQL... ");
    if (mySQL_connection.connect(sql_addr, 3306, sql_user, sql_pass))
    {
      Serial.println("OK.");
      pMySQL_cursor = new MySQL_Cursor(&mySQL_connection);
    }
    else
    {
      Serial.println("FAILED.");
    }
  }

  return pMySQL_cursor;
}

void send_to_db(void *param)
{
  const char INSERT_QUERY_TEMPLATE[] = "INSERT INTO autoscale.weights (time, sensor0, sensor1, sensor2, sensor3) VALUES ('%s',%ld,%ld,%ld,%ld)";
  char insert_query[1024];
  uint32_t sent_values = 0;
  SensorReader *sensor_reader = (SensorReader *)param;

  Serial << "Task send_to_db started on core " << xPortGetCoreID() << "." << endl;
  while (1)
  {
    // get sample from queue - this will block until a sample is available
    sensorsample_t oldest_sample = sensor_reader->RemoveOldestSample();

    String time_string = oldest_sample.timestamp.formatAsEpoch();
    sprintf(insert_query, INSERT_QUERY_TEMPLATE,
            time_string.c_str(),
            oldest_sample.values[0],
            oldest_sample.values[1],
            oldest_sample.values[2],
            oldest_sample.values[3]);

    MySQL_Cursor *pMySQL_cursor = ensureSQLConnection();

    //bool result_set_available =
    pMySQL_cursor->execute(insert_query);

    sent_values++;

    if (sent_values % 300 == 0)
    {
      Serial << time_string
             << " - total samples sent: " << sent_values
             << endl;
    }
  }
}

SensorReader sensor_reader;

void setup()
{
  Serial.begin(115200);
  Serial << endl
         << "=== AutoScale ===" << endl;

  uint32_t arduino_core_id = xPortGetCoreID();
  Serial << "setup() is running on core " << arduino_core_id << endl;

  // start task that sends samples to the database
  TaskHandle_t *pTaskHandle = nullptr;
  xTaskCreatePinnedToCore(
      send_to_db,     /* Task function. */
      "send_to_db",   /* String with name of task. */
      10000,          /* Stack size in bytes. */
      &sensor_reader, /* Parameter passed as input of the task */
      1,              /* Priority of the task. */
      pTaskHandle,    /* Task handle. */
      1 - arduino_core_id);
  //configASSERT(pTaskHandle);
}

NTPClock ntpClock(udp);

void loop()
{
  ensureWiFiConnection();

  // update and query the clock (this might trigger asynchronous NTP traffic)
  ufixedpoint64_t timestamp = ntpClock.tick();

  // can't take meaningful samples without knowing the time
  if (ntpClock.isSynced())
  {
    // do the actual measurement and enqueue the sample
    sensor_reader.AcquireSample(timestamp);
  }
}
