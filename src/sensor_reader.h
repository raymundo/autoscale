#pragma once

#include <HX711-multi.h>

struct ufixedpoint64_t;

struct sensorsample_t
{
  ufixedpoint64_t timestamp;
  long values[4];
};

class SensorReader
{
private:
  // Pins to the load cell amp
  static const int CLK = 22;   // clock pin to the load cell amp
  byte DOUTS[4] = {23, 19, 18, 5}; // data pins to the first through fourth lca
  static const int CHANNEL_COUNT = sizeof(DOUTS) / sizeof(byte);
  long int results[CHANNEL_COUNT];
  HX711MULTI hx711multi;

  uint32_t acquired_samples = 0;
  xQueueHandle queue_handle = nullptr;

public:
  SensorReader();
  virtual ~SensorReader();
  void AcquireSample(ufixedpoint64_t current_timestamp_as_unix_epoch);
  uint32_t TotalAcquiredSamples() const;
  sensorsample_t RemoveOldestSample();
};
